import * as _ from "lodash";

class Differential {
  constructor(
    private readonly _diffMap: Map<number, number>,
    private readonly _sizeDiff: number
  ) {}

  public get diffMap() {
    return this._diffMap;
  }

  public get sizeDiff() {
    return this._sizeDiff;
  }

  public allIndices = (): Array<number> => Array.from(this._diffMap.keys());

  public get = (indx: number): number => Number(this._diffMap.get(indx));

  public getOrDefault = (indx: number, defaultValue: number): number =>
    this._diffMap.has(indx) ? Number(this._diffMap.get(indx)) : defaultValue;

  public mergeWith = (newdiff: Differential): Differential => {
    const mergedSizeDiff = this.sizeDiff + newdiff.sizeDiff;
    const mergedMap = new Map<number, number>();

    const currentEntries = Array.from(this.diffMap.entries());
    const newEntries = Array.from(newdiff.diffMap.entries());

    currentEntries.forEach(([index, val]) => mergedMap.set(index, val));
    newEntries.forEach(([index, val]) => {
      if (index <= mergedSizeDiff) {
        mergedMap.set(
          index,
          mergedMap.has(index) ? Number(mergedMap.get(index)) + val : val
        );
      }
    });
    return new Differential(mergedMap, mergedSizeDiff);
  };

  public readableDiffMap = (): Array<{ index: number; value: number }> => {
    const diffObject = new Array<{ index: number; value: number }>();
    this._diffMap.forEach((val, key) =>
      diffObject.push({ index: key, value: val })
    );
    return diffObject;
  };
}

export { Differential };
