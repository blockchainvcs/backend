class User {
  constructor(
    private readonly _id: number,
    private readonly _role: number,
    private readonly _signature: number
  ) {}

  public get id() {
    return this._id;
  }

  public get role() {
    return this._role;
  }

  public get signature() {
    return this._signature;
  }
}

export { User };
