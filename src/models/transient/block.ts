import { getSHA256Hash } from "./../../commands/common";
import { Entry } from "./entry";

class Block {
  constructor(id: number, entries: Array<Entry>, verifierId: number) {
    this._id = id;
    this._entries = entries;
    this._verifierId = verifierId;
    this._timeStamp = Date.now();
    this._hash = this.calculateHash();
  }

  private readonly _id: number;
  private readonly _entries: Array<Entry>;
  private readonly _verifierId: number;

  public get id() {
    return this._id;
  }

  public get verifierId() {
    return this._verifierId;
  }

  public get entries() {
    return this._entries;
  }

  private readonly _timeStamp: number;
  public get timeStamp() {
    return this._timeStamp;
  }

  private readonly _hash: string;
  public get blockHash() {
    return this._hash;
  }

  private calculateHash = () => {
    let strToHash = "";
    this._entries.forEach(
      (entry) => (strToHash = strToHash.concat(entry.hash))
    );
    strToHash = strToHash.concat(this._id.toString());
    strToHash = strToHash.concat(this._verifierId.toString());
    strToHash = strToHash.concat(this._timeStamp.toString());
    return getSHA256Hash(strToHash);
  };
}

export { Block };
