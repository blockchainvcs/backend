import { fastify, FastifyRequest } from "fastify";

const server = fastify({ logger: true });

server.register(require("fastify-formbody"));
server.register(require("fastify-websocket"));
server.register(require("fastify-cors"), {
  // put your options here
});

server.register(require("./routes/transient/provenance"));
server.register(require("./routes/transient/doc"));
server.register(require("./routes/transient/visualizeblockchain"));

const startServer = async () => {
  try {
    await server.listen(3000);
    server.log.info("Started server...");
  } catch (err) {
    server.log.error(err);
  }
};

(async () => await startServer())();

// Websocket

const WebSocket = require("ws");
const redis = require("redis");

const REDIS_SERVER = "redis://localhost:6379";
const WEB_SOCKET_PORT = 9000;

var redisClient = redis.createClient(REDIS_SERVER);
redisClient.subscribe("UPDATEDOC");

const wsServer = new WebSocket.Server({ port: WEB_SOCKET_PORT });

wsServer.on(
  "connection",
  function connection(ws: { send: (arg0: any) => void }) {
    redisClient.on("message", function (channel: any, message: any) {
      console.log(channel);
      console.log(message);
      ws.send(message);
    });
  }
);

console.log("WebSocket server started at ws://localhost:" + WEB_SOCKET_PORT);
