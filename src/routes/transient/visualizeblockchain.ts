import {
  FastifyInstance,
  FastifyReply,
  FastifyRequest,
  RouteShorthandOptions,
} from "fastify";
import { visualizeBlockchain } from "../../commands/transient/visualizeBlockchain";

module.exports = function (
  server: FastifyInstance,
  opts: RouteShorthandOptions,
  done: Function
) {
  server.route({
    method: "GET",
    url: "/visualizeblockchain",
    handler: async function (req: FastifyRequest, res: FastifyReply) {
      return visualizeBlockchain();
    },
  });
  done();
};
