import { DocPool } from "../../models/transient/docPool";

const getDoc = (purl: string) => DocPool.getInstance().getDocByPURL(purl);

export { getDoc };
