import { DocPool } from "../../models/transient/docPool";

const getPURLsAndContent = (): Array<{ purl: string; content: string }> => {
  const purls = DocPool.getInstance().getDocPURLs();
  return purls.map((purl: string) => ({
    purl: purl,
    content: DocPool.getInstance().getDocByPURL(purl).fileContent,
  }));
};

export { getPURLsAndContent };
