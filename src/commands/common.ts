import sha256 from "crypto-js/sha256";
import Base64 from "crypto-js/enc-base64";

const getSHA256Hash = (str: string) => Base64.stringify(sha256(str));
export { getSHA256Hash };
